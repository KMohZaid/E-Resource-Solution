import 'package:mysql1/mysql1.dart';

class Mysql {
  static String host = 'blur5oo0x8vjorhr5b6h-mysql.services.clever-cloud.com',
      user = 'uozi2cay5ibe4wiq',
      password = 'rpL2CpDFBYmgdWdHXw47',
      db = 'blur5oo0x8vjorhr5b6h';
  static int port = 3306;
  static MySqlConnection? conn;
  Mysql();
  static void setupConnection() async {
    var settings = ConnectionSettings(
        host: host, port: port, user: user, password: password, db: db);
    conn = await MySqlConnection.connect(settings);
  }

  static Future<Map> registerUser(String username,String email,String password,[String? type]) async {
    try {
      var result = await conn?.query(
          'insert into user (username, email_id, password,type) values (?, ?, ?, ?)',
          [username, email, password, type]);
      return {
        "success":true,
        "result":result
      };
    }on MySqlException catch(e){
      if(e.errorNumber==1062){ // Duplicate entry
        if(e.message.contains("user.username")){
          return {
            "isDuplicate":true,
            "what":"Username"
          };
        }else if(e.message.contains("user.email_id")){
          return {
            "isDuplicate":true,
            "what":"Email"
          };
        }
      }
    }
    return {};
  }

  static Future<Map> loginUser(String email,String password) async {
    try {
      var result = await conn?.query(
          'select * from user where email_id = ? and password = ?;',
          [email, password]);
      if(result!=null && result.isEmpty) return {"invalid":true};
      var row = result?.first; // first element
      return {"username":row?["username"]};
    }on MySqlException catch(e){
      return {"error":e};
    }
  }
}