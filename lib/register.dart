import 'package:e_resource_solution/helper.dart';
import 'package:e_resource_solution/mysql.dart';
import 'package:flutter/material.dart';
import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class MyRegister extends StatefulWidget {
  const MyRegister({Key? key}) : super(key: key);

  @override
  MyRegisterState createState() => MyRegisterState();
}

class MyRegisterState extends State<MyRegister> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/register.png'), fit: BoxFit.cover),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 35, top: 30),
              child: const Text(
                'Create\nAccount',
                style: TextStyle(color: Colors.white, fontSize: 33),
              ),
            ),
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.28),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 35, right: 35),
                      child: const RegisterForm(),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class RegisterForm extends StatefulWidget{
  const RegisterForm({super.key});

  @override
  RegisterFormState createState() => RegisterFormState();
}

class RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();
  final _formController ={
    "username":  TextEditingController(),
    "email": TextEditingController(),
    "password": TextEditingController(),
  };
  @override
  Widget build(BuildContext context) {
    return Form(
        key:_formKey,
      child: Column(
        children: [
          FormBuilderTextField(
            name:"username",
            controller: _formController["username"],
            style: const TextStyle(color: Colors.white),
            decoration: registerInputDecoration("Username"),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
            ]),
          ),
          const SizedBox(
            height: 30,
          ),
          FormBuilderTextField(
            name:"email",
            controller: _formController["email"],
            style: const TextStyle(color: Colors.white),
            decoration: registerInputDecoration("Email"),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
              FormBuilderValidators.email(),
            ]),
            autovalidateMode:
            AutovalidateMode.onUserInteraction,
          ),
          const SizedBox(
            height: 30,
          ),
          FormBuilderTextField(
            name:"password",
            controller: _formController["password"],
            style: const TextStyle(color: Colors.white),
            obscureText: true,
            decoration: registerInputDecoration("Password"),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.minLength(8),
            ]),
            autovalidateMode:
            AutovalidateMode.onUserInteraction,

          ),
          const SizedBox(
            height: 40,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Sign Up',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 27,
                    fontWeight: FontWeight.w700),
              ),
              CircleAvatar(
                radius: 30,
                backgroundColor: const Color(0xff4c505b),
                child: IconButton(
                    color: Colors.white,
                    onPressed: () async {
                      if(!_formKey.currentState!.validate()) return;
                      String username=_formController["username"]!.text,
                      email=_formController["email"]!.text,
                      password=_formController["password"]!.text;

                      var r=await Mysql.registerUser(username, email, password);
                      if (!mounted) return;

                      if(r["isDuplicate"]==true){
                        awesomeSnackbar(context, "Exist!",'${r["what"]} is already used, please use another',ContentType.failure);
                        return;
                      }
                      awesomeSnackbar(context, "Successfully Registered!","Now you can login",ContentType.success);
                      Navigator.pushNamed(context,"login");
                    },
                    icon: const Icon(
                      Icons.arrow_forward,
                    )),
              )
            ],
          ),
          const SizedBox(
            height: 40,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context,"login");
                },
                style: const ButtonStyle(),
                child: const Text(
                  'Sign In',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: Colors.white,
                      fontSize: 18),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

InputDecoration registerInputDecoration(String hintText){
  return InputDecoration(
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: const BorderSide(
          color: Colors.white,
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: const BorderSide(
          color: Colors.black,
        ),
      ),
      hintText: hintText,
      hintStyle: const TextStyle(color: Colors.white),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ));
}