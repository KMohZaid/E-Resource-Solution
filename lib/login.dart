import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:e_resource_solution/helper.dart';
import 'package:e_resource_solution/mysql.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class MyLogin extends StatefulWidget {
  const MyLogin({Key? key}) : super(key: key);

  @override
  MyLoginState createState() => MyLoginState();
}

class MyLoginState extends State<MyLogin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/login.png'), fit: BoxFit.cover),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 35, top: 130),
              child: const Text(
                'Welcome\nBack',
                style: TextStyle(color: Colors.white, fontSize: 33),
              ),
            ),
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 35, right: 35),
                      child: const LoginForm(),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class LoginForm extends StatefulWidget{
  const LoginForm({super.key});

  @override
  LoginFormSate createState() => LoginFormSate();
}

class LoginFormSate extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final _formController ={
    "email": TextEditingController(),
    "password": TextEditingController(),
  };
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          FormBuilderTextField(
            name:"email",
            controller: _formController["email"],
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
                fillColor: Colors.grey.shade100,
                filled: true,
                hintText: "Email",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                )),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
              FormBuilderValidators.email(),
            ]),
            autovalidateMode:
            AutovalidateMode.onUserInteraction,
          ),
          const SizedBox(
            height: 30,
          ),
          FormBuilderTextField(
            name:"password",
            controller: _formController["password"],
            style: const TextStyle(),
            obscureText: true,
            decoration: InputDecoration(
                fillColor: Colors.grey.shade100,
                filled: true,
                hintText: "Password",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                )),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
              FormBuilderValidators.minLength(8),
            ]),
            autovalidateMode:
            AutovalidateMode.onUserInteraction,
          ),
          const SizedBox(
            height: 40,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Sign in',
                style: TextStyle(
                    fontSize: 27, fontWeight: FontWeight.w700),
              ),
              CircleAvatar(
                radius: 30,
                backgroundColor: const Color(0xff4c505b),
                child: IconButton(
                    color: Colors.white,
                    onPressed: () async {
                      if(!_formKey.currentState!.validate()) return;
                      String email=_formController["email"]!.text,
                          password=_formController["password"]!.text;
                      var r = await Mysql.loginUser(email, password);
                      if(!mounted) return;

                      if(r["error"]!=null){
                        awesomeSnackbar(context, "Error!","${r['error']}",ContentType.failure);
                      }
                      else if(r["invalid"]!=null){
                        awesomeSnackbar(context, "Invalid email/password!","Wrong email/password, check it.",ContentType.failure);
                      }
                      else if(r["username"]!=null){
                        awesomeSnackbar(context, "Welcome Back!🙂🎉","",ContentType.success);
                        Navigator.pushNamed(context, "home",arguments: {"username":r["username"]});
                      }
                    },
                    icon: const Icon(
                      Icons.arrow_forward,
                    )),
              )
            ],
          ),
          const SizedBox(
            height: 40,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, 'register');
                },
                style: const ButtonStyle(),
                child: const Text(
                  'Sign Up',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: Color(0xff4c505b),
                      fontSize: 18),
                ),
              ),
              TextButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (_)=>const CupertinoAlertDialog(
                        title: Text("Soon"),
                        content: Text("Coming Soon"),
                      ),
                    );
                  },
                  child: const Text(
                    'Forgot Password',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: Color(0xff4c505b),
                      fontSize: 18,
                    ),
                  )),
            ],
          )
        ],
      ),
    );
  }
}