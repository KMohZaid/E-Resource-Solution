import 'package:e_resource_solution/home.dart';
import 'package:flutter/material.dart';
import 'package:e_resource_solution/login.dart';
import 'package:e_resource_solution/register.dart';
import 'package:e_resource_solution/mysql.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    Mysql.setupConnection();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const MyLogin(),
      routes: {
        'register': (context) => const MyRegister(),
        'login': (context) => const MyLogin(),
        'home': (context) => const Home(),
      },
    );
  }
}